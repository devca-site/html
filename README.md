Frontend for Devca site

To get started make sure you have 'bower' installed

    sudo npm install -g bower

Then install assets

    bower install


# Documentation

You can read the deployment documentation here: http://devca-docs.alteroo.com/push-to-deploy.html
