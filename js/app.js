angular.module('devca', [
    'ngRoute',
    'devca.services',
    'devca.controllers'
    //'ui.bootstrap'
]).config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {templateUrl: 'partials/homepage.html', controller: 'HomePageCtrl'});
    $routeProvider.when('/post/:id',{templateUrl: 'partials/post.html', controller: 'PostCtrl'});
    $routeProvider.when('/event/:id',{templateUrl: 'partials/event.html', controller: 'EventCtrl'});
    $routeProvider.when('/events/',{templateUrl: 'partials/events.html', controller: 'EventsCtrl'});
    $routeProvider.when('/posts/',{templateUrl: 'partials/posts.html', controller: 'PostsCtrl'});
    $routeProvider.when('/schedule/',{templateUrl: 'partials/schedule.html', controller: 'ScheduleCtrl'});
    $routeProvider.when('/presentations/',{templateUrl: 'partials/presentations.html', controller: 'PresentationsCtrl'});
    $routeProvider.when('/presentations/:id',{templateUrl: 'partials/presentation.html', controller: 'PresentationCtrl'});
    $routeProvider.when('/videos/',{templateUrl: 'partials/videos.html', controller: 'VideosCtrl'});
    $routeProvider.when('/videos/:id',{templateUrl: 'partials/video.html', controller: 'VideoCtrl'});
    $routeProvider.otherwise({redirectTo: '/'});
}]);

angular.module('devca.controllers',[]);
