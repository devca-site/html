/**
 * Sample controller for a post
 *
 * In this case it simply retrieves the post object from Plone-APIs
 * and sets the values in the view or the template file.
 */

angular.module('devca.controllers')
    .controller('PostCtrl', ['$q', '$sce', '$scope', '$routeParams', 'PostFactory', 'PostsFactory',
        function ($q, $sce, $scope, $routeParams, PostFactory, PostsFactory) {
            PostFactory.get({id:$routeParams.id}, function(post) {
                $scope.post_content = $sce.trustAsHtml(post.text.data);
                $scope.title = post.title;
                $scope.image_banner = post.inner_banner_image.download;
                $scope.post_date = post.effective;
                $scope.author = post.creators[0];
            }, function(error) {
                /**
                 * TODO: Needs a 404 action
                 */
                $scope.post_content = "POST NOT FOUND";
                $scope.title = "404 - Sorry data not found";
                $scope.image_banner = "";
            });
        }
    ]).controller('PostSmallCtrl', ['$scope','PostFactory', 'PostsFactory',
        function($scope, PostFactory, PostsFactory) {
            $scope.constructs = [];
            PostsFactory.getAll(function(posts) {
                $scope.all_posts = posts.items;
              

                posts.items.map(function(post_elem) {
                    var id = post_elem['@id'].split('posts/')[1];
                    if(id) {
                        PostFactory.get({id: id}, function(success) {

                            $scope.constructs.push({
                                id: id,
                                image: success.image.scales.large.download,
                                desc: post_elem.description,
                                title: post_elem.title,
                                created: success.created,
                                author: success.creators[0]
                            });
                        }, function(fail) {

                        });
                    }
                });
            },function(error) {
                /**
                 * TODO: Needs a 404 action
                 */
            });
        }]);
