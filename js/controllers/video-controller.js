
angular.module('devca.controllers')
    .controller('VideosCtrl', ['$scope', 'VideosFactory',
        function ($scope, VideosFactory) {

            /**
             * Loads the Videos
             */
            VideosFactory.getAll(function(videos) {
                //success
                $scope.everything = videos;
                $scope.videos = videos.items;
            }, function(error) {
                //failure
            });

        }
    ]).controller('VideoCtrl', ['$scope', '$sce','$routeParams', 'VideosFactory',
        function ($scope,$sce, $routeParams, VideosFactory) {
            VideosFactory.get({id: $routeParams.id},
                function(video) {
                    $scope.video = video;
                    $scope.video_object = $sce.trustAsHtml(video.embed);
                    $scope.video_content = $sce.trustAsHtml(video.text.data);
                }, function (error) {
                    $scope.video = {};
                });
        }
    ]);
