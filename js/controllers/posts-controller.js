
angular.module('devca.controllers')
    .controller('PostsCtrl', ['$scope', 'PostsFactory',
        function ($scope, PostsFactory) {

            /**
             * Loads the Posts
             */
            PostsFactory.getAll(function(posts) {
                //success
                $scope.posts = posts.items;
            }, function(error) {
                //failure
            });

        }
    ]);
