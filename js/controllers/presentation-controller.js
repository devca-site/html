
angular.module('devca.controllers')
    .controller('PresentationsCtrl', ['$scope', 'PresentationsFactory',
        function ($scope, PresentationsFactory) {

            /**
             * Loads the presentations
             */
            PresentationsFactory.getAll(function(presentations) {
                //success
                $scope.everything = presentations;
                $scope.presentations = presentations.items;
            }, function(error) {
                //failure
            });

        }
    ]).controller('PresentationCtrl', ['$scope', '$sce','$routeParams', 'PresentationsFactory',
        function ($scope,$sce, $routeParams, PresentationsFactory) {
            PresentationsFactory.get({id: $routeParams.id},
                function(presentation) {
                    console.log(presentation);
                    $scope.presentation = presentation;
                    $scope.presentation_object = $sce.trustAsHtml(presentation.embed);
                    $scope.presentation_content = $sce.trustAsHtml(presentation.text.data);
                    $scope.video_object = $sce.trustAsHtml(presentation.embed_code_for_video);
                }, function (error) {
                    $scope.presentation = {};
                });
        }
    ]);
