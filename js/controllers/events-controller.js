
angular.module('devca.controllers')
    .controller('EventsCtrl', ['$scope', 'EventsFactory',
        function ($scope, EventsFactory) {

            /**
             * Loads the Countries/Events
             */
            EventsFactory.getAll(function(events) {
                //success
                console.log(events);
                $scope.events = events.items;
            }, function(error) {
                //failure
            });

        }
    ]);
