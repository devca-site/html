

angular.module('devca.controllers')
    .controller('HomePageCtrl', ['$scope', 'PartnersFactory', 'CarouselFactory', 'PostsFactory',
        function ($scope, PartnersFactory, CarouselFactory, PostsFactory) {

            /**
             * Loads the sponsor logos at the bottom of the homepage
             */
            PartnersFactory.getAll(function(partners) {
                //success
                $scope.partners = partners.items;
            }, function(error) {
                //failure
            });

            CarouselFactory.getAll(function(success_object) {
                $scope.slides = success_object.items;
            }, function(fail) {
                console.log(fail);
            });
        }
    ]);
