

angular.module('devca.controllers')
    .controller('EventCtrl', ['$sce', '$scope', '$routeParams', 'EventFactory',
        function ($sce, $scope, $routeParams, EventFactory) {

            /**
             * Loads the Event
             */
            EventFactory.get({id:$routeParams.id}, function(event) {
                $scope.sched_embed = $sce.trustAsHtml(event.sched_embed);
                $scope.title = event.title;
                $scope.event_url = event.event_url;
                $scope.description = event.description;
                $scope.partners = event.partners;
                $scope.sponsors = event.sponsors;
                $scope.organizer = event.organizer;
                $scope.contact_email = event.contact_email;
                $scope.contact_name = event.contact_name;
                $scope.contact_phone = event.contact_phone;
                $scope.location = event.location;
                $scope.end = event.end;
                $scope.start = event.start;
                $scope.map = $sce.trustAsHtml(event.map);
            }, function(error) {
                //failure
            });

        }
    ]);
