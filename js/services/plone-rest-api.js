/**
 *
 * Plone REST API interfaces
 */
var services = angular.module('devca.services', ['ngResource']);

var BASE_URL = "https://api.developingcaribbean.org";
var HEADERS = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
};

/**
 * Factory to be used to retrieve all sponsors
 **/
services.factory('SponsorsFactory',function($resource){
    return $resource(BASE_URL+'/sponsors', {}, {
        getAll: {
            method: 'GET',
            headers: HEADERS
        }
    });
});

/**
 * Factory for site index
 */
services.factory('SiteIndexFactory',function($resource){
    return $resource(BASE_URL+'/', {}, {
        create: {
            method: 'GET',
            headers: HEADERS
        }
    });
});

/**
 * Factory for a single post on the website with
 * a unique ID or all available/published posts
 */
services.factory('PostFactory',function($resource){
    return $resource(BASE_URL+'/posts/:id', {}, {
        get: {
            method: 'GET',
            headers: HEADERS,
            params: {id: '@id'}
        },
        getAll: {
            method: 'GET',
            headers: HEADERS
        }
    });
});

/**
 * Factory to get all posts (seems redundant but
 * had issues with the PostFactory)
 **/
services.factory('PostsFactory',function($resource){
    return $resource(BASE_URL+'/posts/aggregator', {}, {
        getAll: {
            method: 'GET',
            headers: HEADERS
        }
    });
});

/**
 * Factory to be used to retrieve all slide images from
 * carousel or just one with specific id
 **/
services.factory('CarouselFactory',function($resource){
    return $resource(BASE_URL+'/slider-images/:id', {}, {
        getAll: {
            method: 'GET',
            headers: HEADERS
        },
        get: {
            method: 'GET',
            headers: HEADERS,
            params: {id: '@id'}
        }
    });
});

/**
 * Factory to be used to retrieve all events
 * or just one with specific id
 **/
services.factory('EventsFactory',function($resource){
    return $resource(BASE_URL+'/events/aggregator', {}, {
        getAll: {
            method: 'GET',
            headers: HEADERS
        }
    });
});
/**
 * Factory for a single event on the website with
 * a unique ID or all available/published event
 */
services.factory('EventFactory',function($resource){
    return $resource(BASE_URL+'/events/:id', {}, {
        get: {
            method: 'GET',
            headers: HEADERS,
            params: {id: '@id'}
        }
    });
});

/**
 * Factory to be used to retrieve all sponsors
 * or just one with specific id
 **/
services.factory('SponsorsFactory',function($resource){
    return $resource(BASE_URL+'/sponsors/:id', {}, {
        getAll: {
            method: 'GET',
            headers: HEADERS
        },
        get: {
            method: 'GET',
            headers: HEADERS,
            params: {id: '@id'}
        }
    });
});

/**
 * Factory to be used to retrieve all partners
 * or just one with specific id
 **/
services.factory('PartnersFactory',function($resource){
    return $resource(BASE_URL+'/regional-partners/:id', {}, {
        getAll: {
            method: 'GET',
            headers: HEADERS
        },
        get: {
            method: 'GET',
            headers: HEADERS,
            params: {id: '@id'}
        }
    });
});


/**
 * Factory to get presentations
 **/
services.factory('PresentationsFactory',function($resource){
    return $resource(BASE_URL+'/presentations/:id', {}, {
        getAll: {
            method: 'GET',
            headers: HEADERS
        },
        get: {
            method: 'GET',
            headers: HEADERS,
            params: {id: '@id'}
        }
    });
});


/**
 * Factory to get videos
 **/
services.factory('VideosFactory',function($resource){
    return $resource(BASE_URL+'/videos/:id', {}, {
        getAll: {
            method: 'GET',
            headers: HEADERS
        },
        get: {
            method: 'GET',
            headers: HEADERS,
            cache: false,
            params: {id: '@id'}
        }
    });
});