/**
 * Created by matjames007 on 6/8/16.
 */
angular.module('devca.controllers', [])
    .directive('carousel',
    function () {
        return {
            templateUrl: '../../partials/carousel.html',
            link: function (scope, element, attr) {
            }
        };
    })
    .directive('navbar', function() {
        return {
            templateUrl: '../../partials/navigation.html',
            link: function (scope, element, attr) {

            }
        }
    })
    .directive('countryListing',
    function () {
        return {
            templateUrl: '../../partials/country-listing.html',
            link: function (scope, element, attr) {
            }
        };
    });
